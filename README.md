![Screenshot preview of the theme "Incandescence" by glenthemes](https://64.media.tumblr.com/66b99480ac1217f9b90559ea67ada30d/305ad70eb0407bb7-2b/s1280x1920/bd9c9372795ddd033f83710aeff5bb4ab8a108c6.gif)

**Theme no.:** 12  
**Theme name:** Incandescence  
**Theme type:** Free / Tumblr use  
**Description:** A fansite-style theme with an optional header with up to 3 information boxes and a sidebar, featuring Bakugou Katsuki from Boku no Hero Academia.  
**Author:** @&hairsp;glenthemes  

<!-- **Release date:** [2015-10-22](https://64.media.tumblr.com/00a78ca916c168d5c9c19d2acfe05951/tumblr_nwndd1dIgW1ubolzro1_540.gif)  -->
**Release date:** 2017-04-20  
**Revamp date:** 2024-09-28  
**Last updated:** 2024-09-28

**Post:** [glenthemes.tumblr.com/post/159796091859](https://glenthemes.tumblr.com/post/159796091859)  
**Preview:** [glenthpvs.tumblr.com/incandescence](https://glenthpvs.tumblr.com/incandescence)  
**Guide:** [dub.sh/incandescence-guide](https://dub.sh/incandescence-guide)  
**Download:** [glen-themes.gitlab.io/thms/12/incandescence](https://glen-themes.gitlab.io/thms/12/incandescence)
