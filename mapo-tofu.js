/*
    CREDITS / RESOURCES USED:
    [#] wait for element: Yong Wang
        stackoverflow.com/questions/5525071/61511955#61511955
    [#] minimal soundcloud player: @shythemes
        shythemes.tumblr.com/post/114792480648
    [#] prevent audios from playing over each other
        stackoverflow.com/a/19792168/8144506
    [#] sticky sidebar: abouolia, blixhavn
        github.com/blixhavn/sticky-sidebar-v2
    [#] shorten note count: @shythemes
        shythemes.tumblr.com/post/156021137818
    [#] shorten timeAgo: @bychloethemes, @lushwave
        lushwave.tumblr.com/post/738426605346652160
    [#] tippy.js tooltips: atomiks
        atomiks.github.io/tippyjs/v6
*/

/*------- GET ROOT -------*/
window.getRoot = (VAR) => {
  return getComputedStyle(document.documentElement).getPropertyValue(VAR).replaceAll('"','').replaceAll("'","").trim();
}

/*------- SET ROOT -------*/
window.setRoot = (VAR_NAME, VAR_VAL) => {
  document.documentElement.style.setProperty(VAR_NAME,VAR_VAL);
}

/*-------- WAIT FOR ELEMENT --------*/
// credit: Yong Wang
// stackoverflow.com/questions/5525071/61511955#61511955
window.waitForElement2 = (e) => {
  return new Promise(r=>{if(document.querySelector(e))return r(document.querySelector(e));let t=new MutationObserver(o=>{document.querySelector(e)&&(t.disconnect(),r(document.querySelector(e)))});t.observe(document.body,{childList:!0,subtree:!0})})
}

/*-------- URL PATHNAME --------*/
let currentURL = window.location.href.replace(/[?#].*|\/$/,"");
let currentPath = window.location.pathname.replace(/\/$/,"");
document.documentElement.setAttribute("current-path",currentPath);

/*------ AWAIT IMAGE(S) LOADING ------*/
const loadImage = (url) => new Promise((resolve, reject) => {
  const img = new Image();
  img.addEventListener("load", () => resolve(img));
  img.addEventListener("error", (err) => reject(err));
  img.src = url;
});

/*-------- SCREEN DIMENSIONS --------*/
const screenDims = () => {
  let vpw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  let vph = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
  setRoot("--100vw",`${vpw}px`);
  setRoot("--100vh",`${vph}px`);
  setRoot("--1vw",`${vpw*0.01}px`);
  setRoot("--1vh",`${vph*0.01}px`);
}

/*-------- GET SPEED --------*/
const getSpeed = (s) => {
  let res;
  let nums = Number(s.replace(/[^\d\.]*/g,""));
  let units = s.toLowerCase().replace(/[^a-z]/g,"");
  units == "s" ? res = nums*1000 : res = nums;
  return res
}

/*------- "NOT FOUND" PAGES -------*/
(() => {
  let new_xhr = new XMLHttpRequest();
  new_xhr.open("GET", window.location.href, true);  
  new_xhr.addEventListener("load", () => {
    new_xhr.status === 404 ? document.documentElement.setAttribute("not-found-page","") : null
  })
  new_xhr.send();
})();

/*-------- AUDIO VOLUME --------*/
let audVol = getRoot("--Audio-Post-Volume");
audVol = audVol.trim() == "" ? 1 : audVol.indexOf("%") > -1 ? parseFloat(audVol) / 100 : 1;

/*------- REMOVE HREF.LI LINK REFERRER -------*/
/*
  shoutout: based on / inspired by @magnusthemes:
  v1: magnusthemes.tumblr.com/post/162657076440
  v2: magnusthemes.tumblr.com/post/643460974823833600
*/
const noHrefLi = () => {
  document.querySelectorAll("a[href*='href.li/?']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("https://href.li/?") > -1){
      a.href = href.replace("https://href.li/?","")
    }
  })

  // EXAMPLE: https://t.umblr.com/redirect?z=https%3A%2F%2Fgithub.com%2Feramdam%2FGifsOnTumblr&t=MDU4MDAzOTZkMmM5NmIzNmNjNzJmM2NlNzRkZjY1NWRlYTQyNGM3NiwwZmQwNjM0ODY0ZDBkY2ZlNWVmMjBhNjgyNTJlMDkzNTU2MTUwZTEy
  // SEEN IN THIS POST: felicitysmoak.tumblr.com/post/188159105501
  document.querySelectorAll("a[href*='t.umblr.com/redirect?z=']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("t.umblr.com/redirect?z=") > -1){
      let de = decodeURIComponent(href);
      a.href = de.replace("https://t.umblr.com/redirect?z=","").split("&t=")[0]
    }
  })
}

/*------- REMOVE EMPTY ELEMENTS -------*/
const removeEmptyStuff = () => {
  document.querySelectorAll(".comment-body")?.forEach(p => {
    Array.from(p.childNodes)?.forEach(node => {
      if(node.nodeType === 3 && node.data.trim().length){
        let span = document.createElement("span");
        node.before(span);
        span.appendChild(node);
      }
    })
    
    p.querySelectorAll("p > br:only-child")?.forEach(br => {
      // br.classList.add("tbd");
      br.parentNode.remove()
    })
  })

  setTimeout(() => {
    let stuff = ".comment-body > p:first-child, .comment-body, .comment, .comments, .text-block";

    document.querySelectorAll(stuff)?.forEach(el => {
      if(el.innerHTML.trim() == ""){
        el.remove()
      }
    })

    document.querySelectorAll(".text-block")?.forEach(el => {
      if(el.innerHTML.trim() == ""){
        el.remove()
      }
    })
  },0)
  
  setTimeout(() => {
    document.querySelectorAll(".kjbsjeotzq")?.forEach(z => z.remove())
  },0)
}

/*------- CHANGE .gifv TO .gif -------*/
const removeGIFv = () => {
  document.querySelectorAll("img[src$='.gifv']")?.forEach(img => {
    img.src = img.src.slice(0,-1);
  })
}

/*------- WRAP STRAY TEXT NODES -------*/
const commentBodyNodes = () => {
  document.querySelectorAll(".comment-body, .desc-inner, .box-body-inner")?.forEach(el => {
    let stack = [el];

    while(stack.length > 0){
      // get latest child from stack
      let currentNode = stack.pop();

      // currentNode does NOT have child nodes + is a text node + isn't empty
      if(currentNode.nodeType === 3 && currentNode.data.trim().length > 0){
        let span = document.createElement("span");
        currentNode.before(span);
        span.appendChild(currentNode);
      } else if(currentNode.childNodes.length > 0){
        // currentNode HAS child nodes, add them to the stack
        for(let i=currentNode.childNodes.length-1; i>=0; i--){
          stack.push(currentNode.childNodes[i]);
        }
      }
    }
  })
}

/*------- NPF POST TITLES -------*/
// tumblr.com/glen-test/737099763156123648
const npfPostTitles = () => {
  document.querySelectorAll(".text-block > .comments:first-child > .comment:first-child .comment-body > *:first-child")?.forEach(firstChild => {
    let textBlock = firstChild.closest(".text-block");
    if(firstChild.matches("h1") || firstChild.matches("h2")){
      firstChild.classList.add("post-title");
      
      // demo.tumblr.com/post/718863891534413824
      // in these cases, prepending the title will cause any immediate-subsequent npfs to also prepend since that npf will become the first item in the comment body,
      // so add an invisible layer right before it, then remove it after the page is finished loading
      let next = firstChild.nextElementSibling;
      if(next){
        if(next.matches(".npf_group")){
          let z = document.createElement("div");
          z.classList.add("kjbsjeotzq");
          z.ariaHidden = "true";
          z.hidden = true;
          next.before(z);
        }
      }
      
      textBlock.prepend(firstChild);
    }
  })
}

/*------- NPF FIRST MEDIA -------*/
const npfFirstMedia = () => {
  let o = Date.now();
  let k = setInterval(() => {
    if(Date.now() - o > 1500){
      clearInterval(k)
    } else {
      if(document.querySelector(".photo-origin:first-child")){
        clearInterval(k);
        document.querySelectorAll(".photo-origin:first-child")?.forEach(m => {
          !m.matches(".post-media") ? m.classList.add("post-media") : ""
        })
      }
    }
  },0)
}

/*------- LEGACY PHOTO(SET)S -------*/
function legacyIMG(w, h, ld, hd){
  this.width = Number(w);
  this.height = Number(h);
  this.low_res = ld;
  this.high_res = hd;
}

const legacyPhotos = () => {
  let old_photoset = document.querySelectorAll(".legacy-photoset[layout]:not([layout=''], .done)");
  old_photoset?.forEach((sett) => {
    let sett_ly = sett.getAttribute("layout");
    let sett_ar = [];
    let sett_xy = sett_ly.split("");

    sett_xy?.forEach(zett => {
      sett_ar.push(zett)
    })

    sett_ar.forEach((xett, i) => {
      let pylnn = document.createElement("div");
      pylnn.classList.add("layout-row");
      pylnn.setAttribute("cols", xett);
      pylnn.style.gridTemplateColumns = `repeat(${xett}, 1fr)`
      sett.append(pylnn);
    });

    sett.querySelectorAll(".layout-row:empty").forEach((lr, i) => {
      let curNum = parseInt(lr.getAttribute("cols"));
      sett.querySelectorAll("img").forEach((boo, i) => {
        i += 1;
        if(i <= curNum){
          lr.append(boo);
        }

        loadImage(boo.src).then(i => {
          let h = boo.offsetHeight;
          boo.setAttribute("offset-height",h)
        }).catch(err => console.error(err));
      });
    });
    
    sett.classList.add("done");

  }); // end .legacy-photoset forEach
  
  // legacy photoset row heights
  document.querySelectorAll(".layout-row[cols]")?.forEach(row => {
    let a = Date.now();
    let b = 6000;
    let c = setInterval(() => {
      if(Date.now - a > b){
        clearInterval(c)
      } else {
        if(!row.querySelector("img:not([offset-height]:not([offset-height='']))")){
          clearInterval(c);
          let cols = Array.from(row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])"));
          let minHeight = Math.min(...cols.map(col => Number(col.getAttribute("offset-height"))));
          row.style.height = `${minHeight}px`;
          row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])")?.forEach(i => {
            i.style.marginTop = `calc(0px - (${i.getAttribute("offset-height")}px - ${minHeight}px) / 2)`
          })
          row.classList.add("done");
          legacyPhotosHeights()
        }
      }
    })
  })
  
  // legacy lightboxes
  document.querySelectorAll(".legacy-photo, .legacy-photoset")?.forEach(sel => {
    let mbusv = [];
    sel.querySelectorAll("img:not(a[onclick*='Tumblr.PanoLightboxInit'] > img)").forEach((imgz, index) => {
      mbusv.push(new legacyIMG(
        imgz.getAttribute("width"),
        imgz.getAttribute("height"),
        imgz.src,
        imgz.src
      ));

      let bipuq = Math.floor(parseInt(index)+1);
      imgz.setAttribute("index",bipuq);
      sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},1)`);
      imgz.addEventListener("click", () => {
        sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},${bipuq})`);
      })
    })
  })
}//end legacyPhotos

/*------- LEGACY VIDEOS -------*/
const legacyVideos = () => {
  document.querySelectorAll(".legacy-video .tumblr_video_container + .poster-thumb[url]:not([url=''])")?.forEach(poster => {
    let posterURL = poster.getAttribute("url");
    if(posterURL.trim() !== ""){
      let segment = posterURL.trim().split(".media.tumblr.com/tumblr_")[1].split("_")[0];

      let vidURL = `https://va.media.tumblr.com/tumblr_${segment}.mp4`
      fetch(vidURL).then(q => {
        if(!q.ok){
          // url does not exist
        }
        return q;
      }).then(c => {
        let newVid = document.createElement("video");
        newVid.src = vidURL;
        newVid.setAttribute("poster",posterURL);
        poster.after(newVid);

        if(typeof VIDYO === "function"){
          VIDYO({
            video: "video",
            volume: getRoot("--Video-Post-Base-Volume") || 1
          })
        }

        poster.previousElementSibling.remove();
        poster.remove();
      }).catch(err => console.error(err))
    }
  })
}

/*------- QUOTE STUFF -------*/
const quoteStuff = () => {
  // glen-test.tumblr.com/post/748319112184430592
  document.querySelectorAll("[post-type='text'] .text-block .comment:first-of-type .comment-body > blockquote:first-child + p:last-child")?.forEach(peas => {
    let bbq = peas.previousElementSibling;
    let bbqFirstChild = bbq.querySelector(":scope > *:first-child");
    if(bbqFirstChild){
      if(bbqFirstChild.matches("div:only-child")){
        // note: bbq is the <blockquote>
        bbq.classList.add("npf_quote");
        bbq.classList.add("quote-text");

        if(peas.innerHTML.trim() !== ""){
          peas.classList.add("quote-source")
        }

        let reblogContent = peas.closest(".comment-body");
        if(reblogContent){
          let quoteSet = document.createElement("div");
          quoteSet.classList.add("quote-set");
          reblogContent.prepend(quoteSet);
          reblogContent.querySelectorAll(":scope > *:not(.quote-set)")?.forEach(v => {
            quoteSet.append(v)
          })
        }        
      }
    }
  })
  
  // original post quotes
  document.querySelectorAll("[post-type='text'] .text-block .comment:first-of-type .comment-body > p.npf_quote:first-child + p:last-child")?.forEach(peas => {
    let bbq = peas.previousElementSibling;
    
    // note: bbq is the p.npf_quote
    bbq.classList.add("quote-text");
    let quoteText = bbq.innerHTML;
    let newBQ = document.createElement("blockquote");
    newBQ.innerHTML = quoteText;
    newBQ.classList.add("npf_quote");
    newBQ.classList.add("quote-text");
    bbq.before(newBQ);
    bbq.remove();

    if(peas.innerHTML.trim() !== ""){
      peas.classList.add("quote-source")
    }

    let reblogContent = peas.closest(".comment-body");
    if(reblogContent){
      let quoteSet = document.createElement("div");
      quoteSet.classList.add("quote-set");
      reblogContent.prepend(quoteSet);
      reblogContent.querySelectorAll(":scope > *:not(.quote-set)")?.forEach(v => {
        quoteSet.append(v)
      })
    }
      
  })
}//end quoteStuff()

// call this on resize only
const legacyPhotosHeights = () => {
  document.querySelectorAll(".photo-block .layout-row[cols]")?.forEach(row => {
    let cols = Array.from(row.querySelectorAll(":scope > img"));
    let minHeight = Math.min(...cols.map(col => col.offsetHeight));
    row.style.height = `${minHeight}px`;
    row.querySelectorAll(":scope > img")?.forEach(i => {
      i.style.marginTop = `calc(0px - (${i.offsetHeight}px - ${minHeight}px) / 2)`
    })
  })
}

/*------- CHAT STUFF -------*/
const chatStuff = () => {
  document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
    let contents = chat.childNodes;
    if(contents){
      let textNodes = Array.from(contents).filter((node) => {
        return node.nodeType === 3 && node.data.trim().length > 0;
      });

      textNodes.forEach(node => {
        let wrapper = document.createElement("span");
        node.parentNode.insertBefore(wrapper, node);
        wrapper.appendChild(node);
      });
    }

    /*------- CHAT LABELS -------*/
    // YES chat label
    if(chat.querySelector("b")){
      let checkB = chat.querySelectorAll(":scope > b");
      checkB?.forEach(b => {
        if(!b.matches(".chat-label")){
          b.classList.add("chat-label");
        }
      })
    }

    // NO chat label? force it
    // (make one & it will be empty)
    else {
      let makeB = document.createElement("b");
      makeB.classList.add("chat-label");
      chat.prepend(makeB)
    }

    /*------- CHAT CONTENT -------*/
    chat.querySelectorAll(":scope > b.chat-label")?.forEach((label, i) => {
      let next = label.nextElementSibling;
      
      // if it HAS a .next() element, put all of that into .chat-content
      if(next){
        let elle = "*:not(.chat-label)";
        label.parentNode.querySelectorAll(`:scope > ${elle}`)?.forEach(el => {
          let prevEl = elle.previousElementSibling;
          if(!prevEl?.matches(elle)){
            if(!el.closest(".chat-content")){
              let cont = document.createElement("span");
              cont.classList.add("chat-content");
              el.before(cont);

              let apres = cont.nextElementSibling;
              while(apres && apres.matches(elle)){
                cont.appendChild(apres);
                apres = cont.nextElementSibling;
              }
            }
          }
        })
      }      
      
      // no .next()
      // .chat-content is EMPTY
      else {
        let sp = document.createElement("span");
        sp.classList.add("chat-content");
        label.after(sp);
      }
    })
    
    /*------- WRAP EACH LINE -------*/
    chat.querySelectorAll(":scope > .chat-label + .chat-content")?.forEach(content => {
      let chatContent = content;
      let chatLabel = content.previousElementSibling;
      
      let line = document.createElement("li");
      line.classList.add("chat-line");
      
      chatLabel.before(line);
      
      let next = line.nextElementSibling;
      while(next && (next.matches(".chat-label") || next.matches(".chat-content"))){
        line.append(next);
        next = line.nextElementSibling;
      }
    })
    
    /*------- REPLACE CHAT-WRAP -------*/
    setTimeout(() => {
      // make a new wrapper
      let newWrap = document.createElement("ul");
      newWrap.classList.add("chat-wrap");
      chat.before(newWrap);
      
      chat.querySelectorAll(":scope > .chat-line")?.forEach(line => {
        newWrap.append(line);
      })
      
      /*------- CLEAN UP THE CHAT LINES -------*/
      newWrap.querySelectorAll(".chat-line")?.forEach(line => {
        let label = line.querySelector(".chat-label");
        let content = line.querySelector(".chat-content");
        
        let labelText = label.textContent.trim();
        let contentText = content.textContent.trim();

        // IF: LABEL TEXT BUT EMPTY CONTENT, USE ITS TEXT AS THE CHAT CONTENT
        // only if if the label text doesn't end with a ":"
        if(label.innerHTML.trim() !== "" && content.innerHTML.trim() == ""){
          if(label.textContent.trim().slice(-1) !== ":"){
            content.innerHTML = label.innerHTML;
            label.innerHTML = ""
          }
        }

        // if label and chat BOTH have text in it,
        // but label does not end with :
        // but content starts with :
        // make label end with : instead
        if(contentText.slice(0,1) == ":"){
          if(labelText.slice(-1) !== ":"){
            label.append(":");
            content.textContent = content.textContent.slice(1)
          }
        }
        
        // chat content: make sure it starts with a space
        if(contentText.slice(0,1) !== " "){
          content.innerHTML = " " + content.innerHTML;
        }
        
        // chat content: if it has a <br>, force it onto a new line
        // example: file.garden/ZRt8jW_MomIqlrfn/screenshots/zxfrn.png
        let br = line.querySelector("br");
        while(br){
          let stuffAfterBR = br.nextElementSibling;
          
          let newLine = document.createElement("li");
          newLine.classList.add("chat-line");
          
          let newLabel = document.createElement("b");
          newLabel.classList.add("chat-label");
          
          let newContent = document.createElement("span");
          newContent.classList.add("chat-content");
          
          line.parentNode.append(newLine);
          newLine.append(newLabel);
          newLine.append(newContent);
          
          while(stuffAfterBR && stuffAfterBR.innerHTML.trim() !== ""){
            newContent.append(stuffAfterBR);
            stuffAfterBR = br.nextElementSibling;
          }
          
          br.remove();
          br = line.querySelector("br");
        }
      })//end line forEach
      
      /*------- DEAL W/ CASES OF .chat-wrap + .chat-wrap -------*/
      // example post: tumblr.com/glen-test/737075029801598976
      document.querySelectorAll(".chat-wrap:first-of-type:not(:only-of-type)")?.forEach(firstChat => {
        let next = firstChat.nextElementSibling;
        while(next && next.matches(".chat-wrap")){
          next.querySelectorAll(":scope > .chat-line")?.forEach(line => {
            firstChat.append(line)
          })
          next.remove();
          next = firstChat.nextElementSibling;
        }
      })

      // remove the original chat
      // keep this at the end / don't put anything after this
      chat.remove();
    },0);
  })//end chat forEach
  
  /*------- FURTHER NPF CHAT CLEANUP -------*/
  setTimeout(() => {
    // if chat is the first .comment, hide the comment header
    document.querySelectorAll("[post-type='chat'] .comment:first-child ul.chat-wrap")?.forEach(c => {
      c.closest(".comment")?.classList.add("chat-first-comment")
    })
    
    // remove empty chat stuff after parsing [1/2]
    document.querySelectorAll(".chat-label, .chat-content")?.forEach(c => {
      c.innerHTML.trim() == "" ? c.remove() : ""
    })
    
    // remove empty chat stuff after parsing [2/2]
    document.querySelectorAll(".chat-line")?.forEach(c => {
      c.innerHTML.trim() == "" ? c.remove() : ""
    })
  },0)

}//end chatStuff()

/*------- NPF LINK STUFF -------*/
const npfLinkStuff = () => {
  document.querySelectorAll("[post-type='text'] .post-body .comments .comment-body .npf-link-block")?.forEach(linkBlock => {
    let titleText = "";
    let descText = "";
    let imgURL = "";
    let linkTarget = "";
    let linkHref = "";
    let siteName = "";

    let titleDiv = linkBlock.querySelector(".title");
    titleDiv ? titleText = titleDiv.textContent : null;

    let descDiv = linkBlock.querySelector(".description");
    descDiv ? descText = descDiv.textContent : null;

    let siteDiv = linkBlock.querySelector(".site-name");
    siteDiv ? siteName = siteDiv.textContent : null;

    if(linkBlock.matches(".has-poster")){
      let imgDiv = linkBlock.querySelector(".poster[style*='background-image:url(']");
      imgURL = imgDiv.style.backgroundImage.replace(/(^url\()|(\)$|[\"\'])/g,"");
    }

    let get_a = linkBlock.querySelector(":scope > a");
    if(get_a.matches("[target]")){
      linkTarget = get_a.getAttribute("target");
    }
    if(get_a.matches("[href]")){
      linkHref = get_a.getAttribute("href");
    }

    let makeA = document.createElement("a");
    makeA.classList.add("npf_inst");
    makeA.classList.add("npf_link");
    makeA.classList.add("link-render");
    makeA.target = linkTarget;
    makeA.href = linkHref;

    let h2 = document.createElement("h2");
    h2.textContent = titleText.trim();

    let arrow = document.createElement("i");
    arrow.classList.add("iconcino");
    arrow.setAttribute("stroke","2");
    arrow.setAttribute("icon-name","arrow-forward-double");

    let div = document.createElement("div");
    div.classList.add("site-name");

    let i = document.createElement("i")
    i.classList.add("iconcino");
    i.setAttribute("stroke","2");
    i.setAttribute("icon-name","link-1");

    let span = document.createElement("span");
    span.textContent = siteName.trim();

    linkBlock.before(makeA);
    makeA.append(h2)
    h2.append(arrow);
    makeA.append(div);
    div.append(i);
    div.append(span);

    linkBlock.remove();

  })//end npf link forEach

  document.querySelectorAll(".link-block a.link-render > h2:first-of-type")?.forEach(h2 => {
    h2.innerHTML = h2.innerHTML.trim();
  })
  
  let elle = ".npf_link";
  document.querySelectorAll(elle)?.forEach(el => {
    let prevEl = el.previousElementSibling;
    if(!prevEl?.matches(elle)){
      let cont = document.createElement("div");
      cont.classList.add("npf_group");
      el.before(cont);

      let apres = cont.nextElementSibling;

      !apres || apres && !apres.matches(elle) ? cont.remove() : null;

      while(apres && apres.matches(elle)){
        cont.appendChild(apres);
        apres = cont.nextElementSibling;
      }
    }
  })
}//end npfLinkStuff

/*------- NPF VIDEO STUFF -------*/
const npfVideoStuff = () => {
  document.querySelectorAll(".tmblr-full video[autoplay]")?.forEach(v => {
    v.autoplay = false;
    v.muted = false;
  })
  
  if(typeof VIDYO === "function"){
    VIDYO({
      video: "video",
      volume: getRoot("--Video-Post-Base-Volume") || 1
    })
  }
}

/*------- ASK POST STUFF -------*/
const askStuff = () => {
  document.querySelectorAll(".answer-block .q-top")?.forEach(q => {
    if(q.textContent.trim() !== "" && q.textContent.trim().indexOf(" ") > -1){
      let qtext = q.textContent.trim();
      let askerName = qtext.split(" ")[0];
      let img = q.closest(".answer-block").querySelector(".askerpic");
      img ? img.alt = `${askerName}'s avatar'` : null
    }
  })
}

/*------- READ MORE STUFF -------*/
const readMoreStuff = () => {
  document.querySelectorAll(".comment-body p > a")?.forEach(a => {
    if(a.matches("a.read_more") || a.textContent.trim() == "Keep reading"){
      a.closest("p").classList.add("keep-reading")
    }
  })

  document.querySelectorAll(".posts .comments .comment.original-comment p.keep-reading")?.forEach(r => {
    let rootURL = r.closest(".posts[root-url][username]");
    if(rootURL){
      let username = rootURL.getAttribute("username").trim();
      rootURL = rootURL.getAttribute("root-url").trim();

      let readMoreLink = r.querySelector(":scope > a[href]");
      if(readMoreLink){
        if(readMoreLink.getAttribute("href") !== rootURL){
          readMoreLink.href = rootURL;

          let head = r.closest(".original-comment").querySelector(".comment-header[href]");
          if(head){
            if(head.getAttribute("href") !== rootURL){
              head.href = rootURL;

              head.querySelector(".username").textContent = username;                

              fetch(`https://api.tumblr.com/v2/blog/${username}/avatar`, {
                method: "GET",
                mode: 'no-cors'
              })
              .then(r => {
                let contentType = r.headers.get("content-type");
                if(contentType && contentType.indexOf("application/json") !== -1){
                  return r.json().then(data => {
                    // is "not found"
                  });
                } else {
                  return r.text().then(text => {
                    head.querySelector("img.userpic").src = `https://api.tumblr.com/v2/blog/${username}/avatar`
                  });
                }
              }).catch(err => console.error(err));
            }
          }
        }
      }
    }
  })
}

/*------- LEGACY AUDIO -------*/
const legacyAudio = () => {
  document.querySelectorAll(".aud-gen .aud-info + .aud-iframe iframe.tumblr_audio_player")?.forEach((aud_embed) => {
    let audPostID = aud_embed.closest("[post-type='audio'][id]").getAttribute("id").replaceAll("post-","");

    let audioIMGArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-cover img");
    let audioPlay = aud_embed.closest(".aud-gen")?.querySelector(".q-play");
    let audioPause = aud_embed.closest(".aud-gen")?.querySelector(".q-pause");
    let audioTextArea = aud_embed.closest(".aud-iframe")?.previousElementSibling;
    let audioDLArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-dl");

    aud_embed.addEventListener("load", () => {
      let contents = aud_embed.contentDocument;

      let audSrc, audTitle, audArtist, audAlbumIMG, audAlbumName;

      // IS NULL
      if(contents === null){
        let useSrc = aud_embed.src;
        let trem = useSrc.split("?audio_file=")[1].split("&")[0];
        audSrc = decodeURIComponent(trem)
      }

      // IS NOT NULL
      else {
        audSrc = contents.querySelector("[data-stream-url]")?.getAttribute("data-stream-url");
        audTitle = contents.querySelector("[data-track]")?.getAttribute("data-track");
        audArtist = contents.querySelector("[data-artist]")?.getAttribute("data-artist");
        audAlbumIMG = contents.querySelector("[data-album-art]")?.getAttribute("data-album-art");
        audAlbumName = contents.querySelector("[data-album]")?.getAttribute("data-album");
      }//end: is NOT null

      // bind audio url to download button
      audioDLArea.href = audSrc;

      // create an audio element
      let newAud = document.createElement("audio");
      newAud.src = audSrc;
      aud_embed.closest(".aud-gen").append(newAud);
      newAud.volume = audVol;

      audioPlay.addEventListener("click", () => {
        if(newAud.paused){
          newAud.play();
        }
      });

      audioPause.addEventListener("click", () => {
        if(!newAud.paused){
          newAud.pause();
        }
      });

      newAud.addEventListener("play", () => {
        audioPlay.style.display = "none";
        audioPause.style.display = "flex";
      });

      newAud.addEventListener("pause", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });

      newAud.addEventListener("ended", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });
    }); //end audio load
  }); // end audio each (legacy)
}

/*------- NPF AUDIO -------*/

const neueAudio = () => {
  let hideEmptyNPFAudioInfo = getRoot("--Hide-Empty-NPF-Audio-Info");
  document.querySelectorAll("figcaption.audio-caption")?.forEach((npfAudio) => {
    // check if there's anything preceding text/content
    let prev = npfAudio.previousElementSibling;

    // check if there's anything after it
    let next = npfAudio.nextElementSibling;

    if(next){
      if(next.innerHTML.trim() == ""){
        npfAudio.classList.add("no-next");
      }
    } else {
      npfAudio.classList.add("no-next");
    }

    if(prev){
      if(prev.innerHTML.trim() == ""){
        npfAudio.classList.add("no-prev");
      }
    } else {
      npfAudio.classList.add("no-prev");
    }

    npfAudio.classList.remove("no-prev");
    npfAudio.classList.remove("no-next");

    let npfAudioDiv = document.createElement("div");
    npfAudioDiv.classList.add("aud-gen");
    npfAudio.before(npfAudioDiv);

    npfAudio.querySelectorAll(":scope > *").forEach((h) => {
      npfAudioDiv.append(h);
    });

    npfAudio.remove();

    npfAudioDiv.querySelectorAll(".tmblr-audio-meta").forEach((m) => {
      m.classList.remove("tmblr-audio-meta");
    });

    let deets = document.createElement("div");
    deets.classList.add("aud-info");

    let oldDeets = npfAudioDiv.querySelector(".audio-details");
    oldDeets.before(deets);
    oldDeets.querySelectorAll(":scope > *").forEach((o) => {
      deets.append(o);
    });

    oldDeets.remove();

    let xyz = document.createElement("div");
    xyz.classList.add("aud-xyz");
    deets.before(xyz);
    xyz.append(deets);

    let npfAudioTitle = npfAudioDiv.querySelector(".title");
    let npfAudioArtist = npfAudioDiv.querySelector(".artist");
    let npfAudioAlbum = npfAudioDiv.querySelector(".album");

    if(npfAudioTitle){
      let titleText = npfAudioTitle.textContent;
      if(titleText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        titleText = "Untitled Track";
      }
      let titleDiv = document.createElement("div");
      titleDiv.classList.add("aud-title");
      titleDiv.textContent = titleText;
      npfAudioTitle.before(titleDiv);
      npfAudioTitle.remove();
    }

    if(npfAudioArtist){
      let artistText = npfAudioArtist.textContent;
      if(artistText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        artistText = "Unknown Artist";
      }
      let artistDiv = document.createElement("div");
      artistDiv.classList.add("aud-artist");
      artistDiv.textContent = artistText;
      npfAudioArtist.before(artistDiv);
      npfAudioArtist.remove();
    }

    if(npfAudioAlbum){
      let albumText = npfAudioAlbum.textContent;
      if(albumText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        albumText = "Unknown Album";
      }
      let albumDiv = document.createElement("div");
      albumDiv.classList.add("aud-album");
      albumDiv.textContent = albumText;
      npfAudioAlbum.before(albumDiv);
      npfAudioAlbum.remove();
    }

    let aftermath = xyz.nextElementSibling;
    if(aftermath){
      if(aftermath.matches("img.album-cover")){
        xyz.before(aftermath); // put <img class='album-cover'> BEFORE .aud-xyz
        aftermath.classList.remove("album-cover");
      } else {
        // if .aud-xyz.next() !== img.album-cover,
        // make it
        let makeTheCover = document.createElement("img");
        makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
        xyz.before(makeTheCover);
      }
    }

    // if there is no .aud-xyz.next(),
    // make img.album-cover
    else {
      let makeTheCover = document.createElement("img");
      makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
      xyz.before(makeTheCover);
    }

    let audCover = document.createElement("div");
    audCover.classList.add("aud-cover");

    if(aftermath?.matches("img.album-cover")){
      aftermath.before(audCover);
      audCover.append(aftermath);
    } else {
      xyz.before(audCover);
      audCover.append(audCover.closest(".aud-gen").querySelector("img"));
    }

    let audCtl = document.createElement("div");
    audCtl.classList.add("aud-ctl");
    audCover.prepend(audCtl);

    // play container
    let cplay = document.createElement("button");
    cplay.classList.add("q-play");
    cplay.ariaLabel = "Play";
    audCtl.append(cplay);

    // play icon
    let playIcon = document.createElement("i");
    playIcon.classList.add("gt-misc-icons");
    playIcon.setAttribute("icon-name","play");
    playIcon.ariaHidden = true;
    cplay.append(playIcon);

    // pause container
    let cpause = document.createElement("button");
    cpause.classList.add("q-pause");
    cpause.ariaLabel = "Pause";
    audCtl.append(cpause);

    // pause icon
    let pauseIcon = document.createElement("i");
    pauseIcon.classList.add("gt-misc-icons");
    pauseIcon.setAttribute("icon-name","pause");
    pauseIcon.ariaHidden = true;
    cpause.append(pauseIcon);

    // audio url
    let audSrc;
    let actualAud = npfAudioDiv.nextElementSibling; // was xyz.nextElementSibling
    if(actualAud){
      if(actualAud.matches("audio")){
        if(actualAud.matches("[src]")){
          audSrc = actualAud.getAttribute("src");
        } else if(actualAud.querySelector("source[src]")){
          audSrc = actualAud.querySelector("source[src]").getAttribute("src");
        }
      }
    }

    // make audio btn <a>
    let a = document.createElement("a");
    a.classList.add("aud-dl");
    a.href = audSrc;
    a.target = "_blank";
    a.ariaLabel = "Download";
    xyz.after(a);

    // make audio dl icon
    let ic = document.createElement("i");
    ic.classList.add("iconcino");
    ic.setAttribute("stroke","2");
    ic.setAttribute("icon-name","download-2");
    a.append(ic);

    // set the volume
    actualAud.volume = audVol;

    // play and pause events
    cplay.addEventListener("click", () => {
      if(actualAud.paused){
        actualAud.play();
      }
    });

    cpause.addEventListener("click", () => {
      if(!actualAud.paused){
        actualAud.pause();
      }
    });

    actualAud.addEventListener("play", () => {
      cplay.style.display = "none";
      cpause.style.display = "flex";
    });

    actualAud.addEventListener("pause", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";
    });

    actualAud.addEventListener("ended", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";

      // autoplay the next audio if there is one
      // npfAudioDiv == .aud-gen
      let figNext = npfAudioDiv.closest(".npf_audio").nextElementSibling; /* was npfAudioDiv.nextElementSibling */
      if(figNext && figNext.matches(".npf_audio")){
        figNext.querySelector(".q-play")?.click();
      }
    });
  }); //end npfAudio each

  setTimeout(() => {
    document.querySelectorAll(".npf_group .npf_inst.npf_audio:first-child")?.forEach(i => {
      let parent = i.parentNode;
      if(!parent.querySelector(":scope > .npf_inst:not(.npf_audio)")){
        i.closest(".npf_group").classList.add("npf-audio-only")
      }
    })
    
    document.querySelectorAll(".npf_inst.npf_audio")?.forEach(au => {
      if(au.querySelector("iframe")){
        au.classList.add("has-iframe")
      }
    })
  },0)
}; //end neueAudio()

/*------- SOUNDCLOUD -------*/
const soundcloudStuff = () => {
  let player_btn_color = getRoot("--Audio-Post-Btns-BG");
  let soundcloud_height = Number(getRoot("--SoundCloud-Player-Height").replace(/[^\d\.]*/g,""));
  let scAlbumShowHide = getRoot("--SoundCloud-Show-Album-Image");
  soundcloud_height = !soundcloud_height ? 116 : soundcloud_height;
  scAlbumShowHide = scAlbumShowHide == "yes" ? "true" : "false"
  // minimalist soundcloud player: @shythemes
  // shythemes.tumblr.com/post/114792480648
  document.querySelectorAll("iframe[src*='soundcloud.com']")?.forEach((sc) => {
    let curSrc = sc.getAttribute("src").split("&")[0];
    sc.src = `${curSrc}&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=${scAlbumShowHide}&amp;origin=tumblr&amp;color=${player_btn_color.split("#")[1]}`;
    (sc.height = soundcloud_height), (sc.width = "100%");

    setTimeout(() => {
      soundcloud_height == 20 ? sc.closest("figure")?.classList.add("sc-short") : null
    },0)
  });
}

/*------- AUDIOS ARE NOT VIDEOS -------*/
const audiosAreNotVideos = () => {
  document.querySelectorAll(".posts[post-type='audio']:not(.ex-npf) .post-body > .video-block:first-child > .legacy-video:first-child")?.forEach(lv => {
    if(lv.firstElementChild && lv.firstElementChild.matches("iframe")){
      let post = lv.closest(".posts");
      let postBody = lv.closest(".post-body");
      let videoBlock = lv.closest(".video-block");
      
      let textBlock = document.createElement("div");
      textBlock.classList.add("text-block");
      videoBlock.after(textBlock);
      
      // move comments outside of ".video-block"
      videoBlock.querySelectorAll(":scope > *")?.forEach(e => {
        if(!e.matches(".legacy-video")){
          textBlock.append(e)
        }
      })      
      
      videoBlock.classList += " npf_group photo-origin npf-audio-only";
      videoBlock.classList.remove("video-block");
      lv.classList += " npf_inst npf_audio";
      lv.classList.remove("legacy-video");
      
      post.classList.add("ex-npf");
    }
    
  })
}

/*------- STOP AUDIOS FROM PLAYING OVER E/O -------*/
// credit: stackoverflow.com/a/19792168/8144506
const audioNoConflict = () => {
  document.addEventListener("play", (e) => {
    let auds = document.querySelectorAll("audio");
    auds?.forEach((a,j) => {
      if(auds[j] != e.target){
        auds[j].pause();
        auds[j].currentTime = 0;
      }
    })
  },true)
}

/*------- SIDEBAR -------*/
// StickySidebar originally by by abouolia,
// v2 by blixhavn: github.com/blixhavn/sticky-sidebar-v2
const sidebarStuff = () => {
  
  if(typeof StickySidebar == "function"){
    let stickySidebarV2 = new StickySidebar(".the-sidebar", {
      containerSelector: ".cont-flex",
      scrollContainer: ".main-scroll-cont",
      innerWrapperSelector: ".sb-inner",
      topSpacing: document.querySelector("#qlhlbpx").offsetHeight, // top bar + sticky top gap
      bottomSpacing: document.querySelector("#yxsqkts").offsetHeight
    });
    
    window.addEventListener("resize", () => {
      stickySidebarV2.updateSticky();
    })
    
    let scrollCont = document.querySelector(".main-scroll-cont");
    
    let hadalThreshold = false;
    let hadalZone = document.querySelector("#qlhlbpx").offsetHeight; // top bar + sticky top gap
    // (actually idk if this affects anything, I'm just praying at this point)

    scrollCont.addEventListener("scroll", () => {
      if(scrollCont.scrollHeight - scrollCont.scrollTop - scrollCont.clientHeight <= hadalZone){
        if(!hadalThreshold){
          stickySidebarV2.updateSticky();
          hadalThreshold = true;
        }
      } else {
        hadalThreshold = false;
      }
    })
  }

}

/*------- INCANDESCENCE STUFF -------*/
const incandescence = () => {
  /* ====== HEADER STUFF ====== */
  let le_header = document.querySelector(`[head-height]:not([head-height="img-height"],[head-height*=":"])`);
  
  let startingScreenHeight = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
  let startingHeaderHeight;
  
  le_header ? startingHeaderHeight = le_header.offsetHeight : "";
  
  if(le_header && !currentPath.includes("customize_preview_receiver.html")){
    // ++++++ CAP HEIGHT WHEN SHORT ++++++ //
    if(parseFloat(le_header.getAttribute("head-height")) < 50){
      window.addEventListener("resize", () => {
        let resizedScreenH = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

        // only min-cap the header height if the screen has been resized to be SHORTER (zoom-in)
        if(startingScreenHeight < 2000 && resizedScreenH < startingScreenHeight){
          le_header.style.setProperty("--starting-header-height",`${startingHeaderHeight}px`);
        }

        // free (reset) min-cap when/if new screen h is zoomed out
        else {
          le_header.style.setProperty("--starting-header-height","");
        }
      })
    }

    // ++++++ CAP HEIGHT WHEN TALL ++++++ //
    else if(parseFloat(le_header.getAttribute("head-height")) >= 50){
      window.addEventListener("resize", () => {
        let resizedScreenH = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

        // only max-cap the header height if the screen has been resized to be TALLER (zoom-out)
        // (condition: screen height on first load is NOT super short)
        if(startingScreenHeight > 500 && resizedScreenH > startingScreenHeight){
          le_header.style.setProperty("--starting-header-height",`${startingHeaderHeight}px`);
        }

        // free (reset) max-cap when/if new screen h is zoomed in
        else {
          le_header.style.setProperty("--starting-header-height","");
        }
      })
    }
  }//end: if header exists and NOT customize page
  
  
  /* ====== HEADER BUTTON ====== */
  let headerBtn = document.querySelector("header .arrow-btn");
  let headerPanel = document.querySelector(".header-boxes-panel");
  
  if(headerBtn){
    let moreLabel = headerBtn.ariaLabel;
    let lessLabel = "Close"
    
    headerBtn.addEventListener("click", () => {
      // button state
      !headerBtn.matches(".btn-clicked") ? headerBtn.classList.add("btn-clicked") : headerBtn.classList.remove("btn-clicked");
      
      // header state
      if(headerPanel){
        !headerPanel.matches(".is-showing") ? headerPanel.classList.add("is-showing") : headerPanel.classList.remove("is-showing");
      }
      
    })//end click
  }
  
  
  /* ====== WHITESPACES 420 ====== */
  let toilet = [
    ".header-box-title",
    ".header-box-body",
    `.infobar [class*="-part"]:not([class*="icon-part"])`,
    ".sb-box > *",
    ".jump-wrap"
  ]
  
  for(let skibidi of toilet){
    document.querySelectorAll(skibidi)?.forEach(x => {
      x.innerHTML = x.innerHTML.trim()
    })
  }
  
  /* ====== REMOVE IF EMPTY 420 ====== */
  let poof = [
    ".header-box-title",
    ".header-box-body",
    ".header-box",
    ".sb-box > *"
  ]
  
  for(let poo of poof){
    document.querySelectorAll(poo)?.forEach(x => {
      x.innerHTML.trim() == "" && x.remove()
    })
  }
  
  /* ====== SHORTEN NOTE COUNT ====== */
  // originally by: @shythemes
  // shythemes.tumblr.com/post/156021137818
  if(document.querySelector("html[shorten-note-count='yes']")){
    document.querySelectorAll(".tnc")?.forEach(n => {
      let t = n.innerHTML.trim().split(" ")[0].replace(/,/g,"");
      if(!isNaN(Number(t))){
        if(t > 999){
          t = Math.floor(t / 100) / 10;
          n.innerHTML = `${t}k notes`
        }
      }
    })
  }
  
  /* ====== POST HEADER: VIA VS SRC ====== */
  document.querySelectorAll(`[via-user]:not([via-user=""])[src-user]:not([src-user=""])`)?.forEach(c => {
    let viaName = c.getAttribute("via-user");
    let srcName = c.getAttribute("src-user");
    
    if(viaName.trim() == srcName.trim()){
      let findVia = c.querySelector(".ph-via");
      if(findVia){
        findVia.remove();
      }
    }
  })
  
  /* ====== SHORTEN TIMEAGO ====== */
  // by: @bychloethemes
  // pure JS version: @lushwave (flipse)
  // lushwave.tumblr.com/post/738426605346652160
  let shortTime = document.querySelectorAll(".tf-timeago-short");

  timeAgo && typeof timeAgo == "function" ? timeAgo(shortTime, {
    time: 'short', // should be 'letter' 'short' or 'word'
    spaces: true, // 'true' adds spaces between words and numbers
    words: false, // 'true' turns numbers to words
    prefix: '',  // adds a prefix. could be '~' or 'about' or 'posted' etc.
    suffix: 'ago' // adds a suffix. could be 'ago' or period, etc.
  }) : null;
  
  /* ====== GUIDE PROMPT POPUP ====== */
  let pooopup = document.querySelector("[guide-prompt-popup]");
  let pooopin = document.querySelector("[guide-prompt-popup] [peapea]");
  let peacont = document.querySelector("[pea-img]");
  let peaimg = document.querySelector("[pea-img] img");
  let peaalt = document.querySelector("[pea-img] .alt-tbc");
  let peaguidelink = document.querySelector("[pea-guide-link]");
  
  if(!currentPath.includes("customize_preview_receiver.html")){
    pooopup.remove();
    document.querySelector("[guide-prompt-mild]")?.remove()
  }
  
  if(pooopup && pooopin && peacont && peaimg && peaalt && peaguidelink){
    pooopup.addEventListener("click", e => {
      if(e.target === pooopup){
        !e.target.matches(".clicked") ? prompt1() : prompt2()
      }
    })
    
    document.addEventListener("keydown", (e) => {
      if(e.keyCode == 27 || e.code === "Escape"){
        pooopup.target.matches(".clicked") ? prompt1() : prompt2()
      }
    })
    
    function prompt1(){
      peacont.style.display = "block";
      
      setTimeout(() => {
        peacont.classList.add("sanders-1");
        
        setTimeout(() => {
          peacont.classList.add("sanders-2");
          
          let a = 500;
          let b = Date.now();
          let c = setInterval(() => {
            if(Date.now() - b > a){
              clearInterval(c)
            } else {
              pooopin.scrollTo(0, pooopin.scrollHeight);
            }
          },0)
          
          setTimeout(() => {
            pooopin.classList.add("nuh-uh");
            
            setTimeout(() => {
              pooopin.classList.remove("nuh-uh");
              peaguidelink.classList.add("woooah");
              pooopup.classList.add("clicked")
              setTimeout(() => {
                peaguidelink.classList.remove("woooah")
              },200)
            },690)
          },500)
        },500)
      },0)
    }//end prompt1()
    
    function prompt2(){
      pooopin.classList.add("nuh-uh");
      setTimeout(() => {
        pooopin.classList.remove("nuh-uh");
        peaguidelink.classList.add("woooah");
        pooopup.classList.add("clicked")
        setTimeout(() => {
          peaguidelink.classList.remove("woooah")
        },200)
      },690)
    }
    
    peaimg.alt = `"I am once again asking for your financial support" Bernie Sanders meme from 2020, with the last part of the sentence modified so that the sentence in its etirety reads: "I am once again asking for you to read the theme guide."`

    peaalt.classList.add("tmblr-alt-text-helper");
    peaalt.classList.remove("alt-tbc");
  }//end: if guide exists
  
  /* ====== CLICK-TOGGLE TUMBLR CONTROLS ====== */
  waitForElement2(`html[tumblr-controls-vis="click-toggle"] iframe.tmblr-iframe.iframe-controls--desktop`).then(tmblr_iframe => {
    let plusBtn = document.querySelector("button.tci");
    if(plusBtn){
      plusBtn.addEventListener("click", e => {
        if(!plusBtn.matches(".active")){
          plusBtn.classList.add("active");
          tmblr_iframe.classList.add("tog-on")
        } else {
          plusBtn.classList.remove("active");
          tmblr_iframe.classList.remove("tog-on")
        }
      })
    }
  })
  
  /* ====== MUSIC PLAYER ====== */
  let mpBarsArea = document.querySelector(".incndscnc-music-player .bars-hgsmd");
  if(mpBarsArea && !mpBarsArea.querySelector(".col-ttkzz")){
    for(let i=1; i<=5; i++){
      let col = document.createElement("div");
      col.classList.add("col-ttkzz");
      mpBarsArea.append(col);
    }
    
    document.querySelectorAll(".col-ttkzz")?.forEach((col,colNum) => {
      colNum += 1;
      
      if(colNum == 1){
        for(let i=1; i<=2; i++){
          let sq = document.createElement("div");
          sq.classList.add("sq-idxrn");
          col.append(sq);
        }
      }
      
      else if(colNum == 2){
        for(let i=1; i<=3; i++){
          let sq = document.createElement("div");
          sq.classList.add("sq-idxrn");
          col.append(sq);
        }
      }
      
      else if(colNum == 3){
        for(let i=1; i<=4; i++){
          let sq = document.createElement("div");
          sq.classList.add("sq-idxrn");
          col.append(sq);
        }
      }
      
      else if(colNum == 4){
        for(let i=1; i<=4; i++){
          let sq = document.createElement("div");
          sq.classList.add("sq-idxrn");
          col.append(sq);
        }
      }
      
      else if(colNum == 5){
        for(let i=1; i<=2; i++){
          let sq = document.createElement("div");
          sq.classList.add("sq-idxrn");
          col.append(sq);
        }
      }
    })//end: bars' col each    
  }//end: bars area exists
  
  /* ====== REMOVE SIDEBAR IMAGE IF EMPTY ====== */
  document.querySelectorAll(`.sb-img[img-input*="assets.tumblr.com/images/x.gif"]`)?.forEach(img => {
    img.remove();
  })

  /* ====== SMOOTH SCROLLING ====== */
  // on by default, but if user wants it off, destroy the instance
  if(!document.documentElement.matches("[smooth-scroll='yes']")){
    SmoothScroll.destroy();
  }
  
}//end incandescence stuff

/*------- REASSIGN POST TYPES -------*/
const redoPostTypes = () => {
  setTimeout(() => {
    
    /*============================================*/
    
    let set = "[post-type='text'] .post-body > .photo-origin";
    let item = ".npf_inst";
    let itemFilterPrefix = "npf_"

    document.querySelectorAll(set)?.forEach(set => {
      let itemNoDot = item.replaceAll(".","");
      let firstClasses;
      let nextChildClasses = [];
      set.querySelectorAll(`:scope > ${item}:first-child`)?.forEach(itemEl => {
        firstClasses = itemEl.classList.value.split(" ");
        firstClasses = firstClasses.filter(x => x !== itemNoDot && x.startsWith(itemFilterPrefix));
        
        if(!itemEl.nextElementSibling){
          set.classList.add("npf-tba-single-type")
        } else {
          while(itemEl.nextElementSibling?.matches(item)){
            let nextEl = itemEl.nextElementSibling;
            let nextClasses = nextEl.classList.value.split(" ");
            nextClasses = nextClasses.filter(x => firstClasses.includes(x));

            // if .next matches any type of npf of the 1st
            if(nextClasses.length){
              // if: all .next matches 1st in some way
              if(!nextEl.nextElementSibling){
                set.classList.add("npf-tba-single-type")
              }

              itemEl = nextEl;
            }

            // doesn't match 1st in any way, terminate
            else {
              set.closest(".posts").setAttribute("post-type","multimedia");
              set.classList.add("npf-multimedia")
              break;
            }
          }//end while
        }
      })
    })
    
    /*---- ONLY 1 TYPE ----*/
    document.querySelectorAll(".npf-tba-single-type")?.forEach(singleType => {
      let post = singleType.closest(".posts");
      let firstC = singleType.firstElementChild;
      
      // photo
      if(firstC?.matches(".npf_photo")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","photo")
      }
      
      // video
      else if(firstC?.matches(".npf_video")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","video")
      }
      
      // audio
      else if(firstC?.matches(".npf_audio")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","audio")
      }
      
      // link
      else if(firstC?.matches(".npf_link")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","link")
      }
      
      singleType.classList.remove("npf-tba-single-type");
    })//end singleType
      
    
    /*============================================*/

    document.querySelectorAll("[post-type='text'] .post-body .comments .comment:first-child .comment-body > *:first-child")?.forEach(firstCommentBody => {
      let post = firstCommentBody.closest("[post-type='text']");

      // poll
      if(firstCommentBody.matches(".poll-post")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","poll")
      }

      // chat
      else if(firstCommentBody.matches(".chat-wrap")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","chat")
      }

      // quote
      else if(firstCommentBody.matches(".quote-set")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","quote")
      }
    })//end: post each
  },0)
}

/*------- CHANGE CURLY QUOTES TO STRAIGHT ONES -------*/
const uncurly = () => {
  document.querySelectorAll("pre, code, .npf_chat, .chat-content")?.forEach((code) => {
    let stuff = code.innerHTML;
    stuff = stuff.replace(/\u201C/g,'"').replace(/\u201D/g,'"')
    code.innerHTML = stuff;
  });
}

/*------- DEACTIVATED USERS -------*/
const deactUsers = () => {
  // reblog heads
  document.querySelectorAll(".comment.deactivated[username]:not([username=''])")?.forEach(user => {
    let username = user.getAttribute("username");
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      let usernameDiv = user.querySelector(".username");
      let deacDiv = user.querySelector(".deactivated");

      usernameDiv ? usernameDiv.textContent = avantDash : null;
    }
  })//end reblog heads (comments)
  
  // answerer
  document.querySelectorAll(".answer-block .comment.original-comment[username]:not([username=''])")?.forEach(user => {
    let answeredLabel = "";

    let username = user.getAttribute("username");
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      let usernameDiv = user.querySelector(".username");
      let deacDiv = user.querySelector(".deactivated");

      if(usernameDiv && !deacDiv){
        answeredLabel = " " + usernameDiv.textContent.split(" ")[1];
        usernameDiv.textContent = avantDash

        let d = document.createElement("span");
        d.classList.add("deactivated");
        d.textContent = "(deactivated)";
        usernameDiv.after(d);
        
        usernameDiv.append(answeredLabel)
      }
    }
  })//end answerer
  
  // post header via, post header src
  document.querySelectorAll(".ph-left .ph-via, .ph-left .ph-src")?.forEach(user => {
    let origText = user.textContent;

    // is deactivated
    if(origText.indexOf("-deac") > -1){
      let avantDash = origText.substring(0,origText.lastIndexOf("-"));

      if(user.classList.value.includes("ph-via")){
        user.closest(".ph-left").setAttribute("via-user",avantDash);
      } else if(user.classList.value.includes("ph-src")){
        user.closest(".ph-left").setAttribute("src-user",avantDash);
      }

      user.textContent = `${avantDash} (deactivated)`
    }

    // is not deactivated
    else {
      if(user.classList.value.includes("ph-via")){
        user.closest(".ph-left").setAttribute("via-user",origText);
      } else if(user.classList.value.includes("ph-src")){
        user.closest(".ph-left").setAttribute("src-user",origText);
      }
    }
  })

  // compare post header via [with] post header src
  document.querySelectorAll(`[via-user]:not([via-user=""])[src-user]:not([src-user=""])`)?.forEach(user => {
    if(user.getAttribute("via-user") == user.getAttribute("src-user")){
      user.classList.add("reblogged-from-src")
    }
  })

  
  // via & src
  document.querySelectorAll(".via-part a, .src-part a")?.forEach(user => {
    let username = user.textContent;
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      user.textContent = `${avantDash} (deactivated)`
    }
  })//end via & src
}

/*------- TOGGLE TAGS -------*/
const togTags = () => {
  document.querySelectorAll(".posts .post-controls .tags-press")?.forEach(t => {
    let post = t.closest(".posts");
    let tagscont = post.querySelector(".tagscont");
    if(tagscont){
      t.addEventListener("click", () => {
        !tagscont.matches(".active") ? tagscont.classList.add("active") : tagscont.classList.remove("active")
      })
    }
  })
}

/*------- OVERSCROLL Y/N -------*/
const overscroll = () => {
  // prevent trackpad overscroll ONLY on desktops
  if(/Mobi|Android/i.test(navigator.userAgent)){
    // on mobile
    document.querySelectorAll("html, body, .main-scroll-cont")?.forEach(s => {
      s.matches(".no-overscroll") ? s.classList.remove("no-overscroll") : null
    })
  }

  else {
    // on desktop
    document.querySelectorAll("html, body, .main-scroll-cont")?.forEach(s => {
      !s.matches(".no-overscroll") ? s.classList.add("no-overscroll") : null      
    })
  }
}

/*------- TURN [title] TO [aria-label] -------*/
const ariaLabels = () => {
  document.querySelectorAll("[title]:not([title=''])")?.forEach(t => {
    let tt = t.title;
    t.removeAttribute("title");
    t.setAttribute("aria-label",tt);
  })
}

/*------- TOOLTIPS -------*/
const tippys = (el) => {
  document.querySelectorAll(`[${el}]:not([${el}=''])`)?.forEach(x => {
    if(x.getAttribute(el).trim() !== ""){
      if(el === "title"){
        let ogTitle = x.getAttribute(el);
        x.addEventListener("mouseenter", () => {
          x.removeAttribute("title")
        })

        x.addEventListener("mouseleave", () => {
          x.setAttribute("title",ogTitle)
        })
      }
      tippy(x, {
        content: x.getAttribute(el),
        maxWidth: "min(var(--Sidebar-Width), var(--Post-Width))",
        followCursor: true,
        arrow: false,
        offset: [0, 24],
        moveTransition: "transform 0.015s ease-out",
      })
    }
  })
}

/*------- FADE IN STUFF AFTER INITIALIZING -------*/
const fadeLoad = () => {
  let vp = getSpeed(getRoot("--Load-In-Delay"));
  let sp = getSpeed(getRoot("--Load-In-Speed"));
  vp = vp === "" ? 0 : vp;
  sp = sp === "" ? 400 : sp;

  document.querySelectorAll(".load-in:not(.load-removing)")?.forEach(el => {
    setTimeout(() => {
      el.classList.remove("load-in");
      el.classList.add("load-removing");
      setTimeout(() => {
        el.classList.remove("load-removing");
        el.getAttribute("class").trim() == "" ? el.removeAttribute("class") : null
      },sp)
    },vp)
  })
}

/*------- THEME INIT -------*/
const themeInit = () => {
  document.querySelectorAll(".tumblr_preview_marker___")?.forEach(m => {
      m.remove();
  })
  
  let theme_markers = ["blogtitle","title","blogdescription"];  
  for(let m of theme_markers){
    document.querySelectorAll(`.tumblr_theme_marker_${m}`)?.forEach(b => {
      b.replaceWith(...b.childNodes);
    })
  }
  
  // housekeeping stuff:
  noHrefLi();
  commentBodyNodes();
  
  // essential theme stuff:
  npfPostTitles();
  npfFirstMedia();
  legacyPhotos();
  legacyVideos();
  quoteStuff();
  chatStuff();
  npfLinkStuff();
  npfVideoStuff();
  askStuff();
  readMoreStuff();
  deactUsers();
  
  // theme audio stuff:
  legacyAudio();
  neueAudio();
  soundcloudStuff();
  audiosAreNotVideos();
  audioNoConflict();
  
  // incandescence stuff
  sidebarStuff()
  incandescence()
  
  // extra theme stuff:
  removeGIFv();
  
  redoPostTypes();
  removeEmptyStuff();
  uncurly();
  
  
  togTags();
  
  if(typeof npfAltText == "function"){
    npfAltText({
      position: getRoot("--NPF-Alt-Text-Button-Position") || "bottom left",
      cornerOffset: getRoot("--NPF-Alt-Text-Button-Corner-Offset") || "15px",
      buttonText: getRoot("--NPF-Alt-Text-Button-Label") || "ALT"
    })
  }
  
  ariaLabels();
  tippys("aria-label");
  
  fadeLoad();
}//end themeInit()

/*------- ON INITIAL LOAD -------*/
document.addEventListener("DOMContentLoaded", () => {
  themeInit();
  overscroll();
})//end DOMContentLoaded

window.addEventListener("resize", () => {
  legacyPhotosHeights();
  screenDims();
})

/*------- NOTES -------*/
/* note: the following CANNOT be 'const' */
function tumblrNotesInserted(){
  ariaLabels();
  tippys("aria-label");
  
  // making sure that "JohnSmith posted this" is always the last item otherwise it's weird lol
  document.querySelectorAll("ol.notes li.original_post")?.forEach(john => {
    if(john.nextElementSibling && john.nextElementSibling.matches("li:not(.more_notes_link_container)")){
      john.remove();
    }
  })
}