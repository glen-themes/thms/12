// wait for element to be present in the DOM
// credit: Yong Wang: stackoverflow.com/a/61511955/8144506
window.waitForElement = function(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}

$(document).ready(function(){
    // wait for .npf_inst, then do stuff
    waitForElement(".npf_inst").then(() => {
      $(".npf_inst").each(function(){
          $(this).css("margin-bottom","");

          if(!$(this).prev("blockquote").length){
              $(this).insertAfter($(this).parent().find("#tagstop"));
          }
      });

      $("[post-type='text'] p").each(function(){
          if($(this).prev(".npf_inst").length){
              $(this).css("margin-top","var(--NPF-Caption-Spacing)")
          }
      });

      $(".capt").each(function(){
          if($(this).text().trim() === "") {
              $(this).remove();
          }
      });

      $(".npf_inst .tmblr-full audio[controls]").each(function(){
          $(this).parents("[post-type='text']").find(".eggtart span.lnr").removeClass("lnr-pilcrow").addClass("lnr-volume-high")
      })
    })//end waitForElement
  
  
  
    // wait for .photo-origin, then do stuff
    waitForElement(".photo-origin").then(() => {

      // original posts (not-reblogs) - npf detection
      $(".posttext > .npf_inst:first-child:not(.photo-origin)").each(function(){
          $(this).addClass("photo-origin")
      })

      // reassign post types
      $("[post-type='text']").each(function(){
          // photo
          let po = $(this).find(".photo-origin");
          if(po.length == 1){
              if(po.find("img").length > 1){
                  $(".eggtart",this).find("span.lnr").removeClass("lnr-pilcrow").addClass("lnr-layers")
              } else {
                  $(".eggtart",this).find("span.lnr").removeClass("lnr-pilcrow").addClass("lnr-camera")
              }
          }

          // polls
          if($(this).find("[data-npf*='poll']").length == 1){
              $(".eggtart",this).find("span.lnr").removeClass("lnr-pilcrow").addClass("lnr-chart-bars")
          }

          // audio: spotify, soundcloud
          if($(this).find("[data-npf*='audio']").length == 1){
              $(".eggtart",this).find("span.lnr").removeClass("lnr-pilcrow").addClass("lnr-volume-high")
          }

          // chat
          if($(this).find("[data-npf*='chat']").length){
              if(!$(this).find("[data-npf*='chat']").eq(0).prev().length){
                  $(".eggtart",this).find("span.lnr").removeClass("lnr-pilcrow").addClass("lnr-bubble")
              }
          }
      })
    })//end waitForElement
})//end ready
